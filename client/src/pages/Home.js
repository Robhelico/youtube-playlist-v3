import React from "react";
import PlaylistManager from "../components/PlaylistManager";

const Home = () => {
  console.log("Home");

  return (
    <div className="home">    
      <PlaylistManager/>
    </div>
  );
};
export default Home;