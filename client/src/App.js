import React from "react";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Home from "./pages/Home";
import Not_Found from "./pages/Not_Found";

class App extends React.Component {

  render () {
    return (
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={Home}></Route>
            <Route component={Not_Found}></Route>
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
  
}

export default App;
