import React, { useState } from 'react';
import { getLocalStorage } from './utilityFunctions';
    
const CreatePlaylist = ({CallbackChangedPlaylistsList}) => {

    //Initialisation
    const [playlistName, setPname] = useState("");
    const [error, setError] = useState(false)

    //Methods
    const handleSubmit = (e) => { //add a new playlist to the playlists list
        e.preventDefault();

        if (playlistName) {
            let newPlaylistsList = getLocalStorage("playlists_list", "arr");

            let i = 0;
            while (i < newPlaylistsList.length && newPlaylistsList[i] !== playlistName){ //not 2 times the same name
                i++;
            }
            
            if (i === newPlaylistsList.length){
                newPlaylistsList = [...newPlaylistsList, playlistName]; //verify if it works with undefined

                localStorage.setItem("playlists_list", JSON.stringify(newPlaylistsList)); //store into localStorage and send back to PlaylistManager
                localStorage.setItem(playlistName, JSON.stringify({name:playlistName, musics:[]}));
                CallbackChangedPlaylistsList(newPlaylistsList);
                setError(false);
                setPname("");
            }
            else{
                setError(true);
            }            
        }
        else {
            setError(true);
        }
    };

    return (
        <div>
        <form onSubmit={(e) => handleSubmit(e)} className="create_playlist">
            <input
                onChange={(e) => setPname(e.target.value)}
                type="text"
                placeholder="Playlist name"
                value={playlistName}
                className="textbar"
                style={{ background: error ? "#ffb3b3" : "#ffdcdc" }}
                />  
            <input type="submit" className="submit" value="+" />{
            error ? <p>wrong name</p> : <p></p>}
        </form>
        </div>
    );
};
export default CreatePlaylist;




