import React, { useState } from 'react';
import { getLocalStorage } from './utilityFunctions';

const Playlist = ({playlistName, index, isSaved, CallbackSelectedPlaylist, CallbackChangedPlaylistsList, CallbackSavedPlaylist, selected, CallbackChangedPlaylist }) => {

    //Initialisation
    const [isEditing, setIsEditing] = useState(false);
    const [editedContent, setEditContent] = useState("");

    //Methods
    const handleSelection = () => { // display the playlist in the right part of the page
        if(!selected){
            if(isSaved || (window.confirm("Confirm ? Remember to save the current playlist before selecting an other one."))){
                const selectedPlaylist = getLocalStorage(playlistName, "playlist"); // update
                localStorage.setItem("selected_playlist", JSON.stringify(selectedPlaylist));
                CallbackSelectedPlaylist(selectedPlaylist);
                CallbackSavedPlaylist(true);
            }            
        }
   };   
   
   const handleEdit = () => { // modify the name of the playlist

        const newName = editedContent ? editedContent : playlistName;
        let currentPlaylistsList = getLocalStorage("playlists_list");

        let i = 0; // finding the index of the music that has to be edited
            while (currentPlaylistsList[i] !== playlistName){
            i++;
        }
        currentPlaylistsList[i] = newName; // re-assign a new name to the playlist

        let newPlaylist = getLocalStorage(playlistName, "playlist");
        newPlaylist.name = newName;

        localStorage.setItem("playlists_list", JSON.stringify(currentPlaylistsList)); //update
        localStorage.removeItem(playlistName);
        localStorage.setItem(newName, JSON.stringify(newPlaylist));
        CallbackChangedPlaylistsList(currentPlaylistsList);     
        CallbackChangedPlaylist(newPlaylist);

        setIsEditing(false);
    };


    const handleRemove = () => { // remove the playlist from the whole app

        let currentPlaylistsList = getLocalStorage("playlists_list");
    
        let index = 0; // finding the index of the music that has to be removed
        while (currentPlaylistsList[index] !== playlistName){
          index++;
        }

        const selectedPlaylist = getLocalStorage("selected_playlist"); // if the playlist removed is displayed in the viewer, display nothing
        if(selectedPlaylist && (selectedPlaylist.name  === playlistName)) {
          localStorage.setItem("selected_playlist", JSON.stringify({name:"",musics:[]}));
          CallbackSelectedPlaylist({name:"",musics:[]});
        }
    
        currentPlaylistsList.splice(index, 1); // playlist removed

        const sendJSON = JSON.stringify(currentPlaylistsList); // update
        localStorage.setItem("playlists_list", sendJSON);
        localStorage.removeItem(playlistName);
        CallbackChangedPlaylistsList (currentPlaylistsList);

    };


    return (
        <div className="playlist_element">
                
            <button onClick={() => {
                handleSelection();}} 
                className={ selected ? "index_selected" : "index" }>
                O
            </button>

            <div className="playlist_name">
                {isEditing ? (
                    <textarea
                    onChange={(e) => setEditContent(e.target.value)}
                    autoFocus
                    defaultValue={editedContent ? editedContent : playlistName}
                    ></textarea>
                ) : (
                    <p>{editedContent ? editedContent : index + " - " +playlistName}</p>
                )}
            </div>
            
            
            {isEditing ? (
            <button className="edit_button" onClick={handleEdit}>Valider</button>
            ) : (
            <button className="edit_button" onClick={() => setIsEditing(true)}>⚙</button>
            )}
              
            <button
                onClick={() => {
                    if (window.confirm("Remove this playlist and all the musics it contains ?")) {
                    handleRemove();
                    }
                }}
                className="remove_button"
                >
                ✖
            </button>
        </div>
    );
};
export default Playlist;
