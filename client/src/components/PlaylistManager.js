import React, { useState, useEffect } from 'react';
import CreatePlaylist from './CreatePlaylist';
import Playlist from './Playlist';
import { urlFormatter, getLocalStorage } from './utilityFunctions';
import Music from "./Music";
import axios from "axios";
import SavePlaylist from "./SavePlaylist"
axios.defaults.headers.common = {
  "Content-Type": "application/json"
}

const PlaylistManager = ({callback}) => {

    //Initialisation
    const [playlistsList, setPlaylistsList] = useState(() => {
        return getLocalStorage("playlists_list", "arr");
    });
    const [selectedPlaylist, setSelectedPlaylist] = useState(() => {
        return getLocalStorage("selected_playlist", "playlist");
    });
    const [playerUrl, setPlayerUrl] = useState (() => {
        return "";
    });
    const [search, setSearch] = useState("");
    const [error, setError] = useState(false);
    const [isSaved, setIsSaved] = useState(() => {
        return getLocalStorage("is_saved","bool");
    })

    //Methods
    const getPlayerURL = _ => { // get the url of the video to be displayed in the player
        const currentUrl = getLocalStorage("current_url", "str");
        const selectedPlaylistMusics = selectedPlaylist.musics;
        
        if(currentUrl !== '' && currentUrl !== '""' && currentUrl !== "" && currentUrl !== null){//if url from localStorage not empty, get it
            setPlayerUrl(currentUrl);
        }
        
        else if (selectedPlaylist.musics[0]) {//if url from localStorage empty but playlist not empty
            setPlayerUrl(urlFormatter( selectedPlaylistMusics[0].url ));
        }    
        else{//else, create new url
            localStorage.setItem("current_url",JSON.stringify(""));
            setPlayerUrl("");
        }  
    };
    

    const handleSubmit = (e) => { // add to the playlist the first youtube result thanks to the server

        e.preventDefault();
        
        if (search) {
            let first_result = {};
            const data_sent = {yt_search : search};

            axios //ask information about the search to the server
            .post(
                "/api/insert",
                data_sent, 
                {headers: { 'Content-type': 'application/json; charset=UTF-8' }}
            )
            .then((res) => {
                first_result = res.data;

                if (!first_result) { //no result for Youtube search
                    setError(true);
                } 
                else { //result for youtube search
                    first_result["date"] = Date.now();
                    const newPlaylist = [...selectedPlaylist.musics, first_result]; // store the music in the playlist

                    const newSelectedPlaylist = ({name:selectedPlaylist.name ,musics:newPlaylist}); //update
                    localStorage.setItem ("selected_playlist", JSON.stringify(newSelectedPlaylist));
                    setSelectedPlaylist(newSelectedPlaylist);
                    setError(false);
                    setSearch("");
                    CallbackSavedPlaylist(false);
                    localStorage.setItem("is_saved", JSON.stringify(false));
                }
            })
            .catch(error => {
                console.log(error)});
        }
        else {
            setError(true);
        }
    };


    //Callbacks
    const CallbackSelectedPlaylist = (newSelectedPlaylist) => {
        localStorage.setItem("current_url", JSON.stringify(""));
        getPlayerURL();
        setSelectedPlaylist(newSelectedPlaylist);
    }
    const CallbackChangedPlaylistsList = (newPlaylistsList) => {
        setPlaylistsList(newPlaylistsList);
    }

    const CallbackUpdatedUrl = (newUrl) => {
        setPlayerUrl(newUrl);
    }
    const CallbackChangedPlaylist = (newPlaylist) => {
        setSelectedPlaylist(newPlaylist);   
    }
    const CallbackSavedPlaylist = (boolean) => {
        setIsSaved(boolean);  
    }

    //Side-effects
    useEffect(() => {
        getPlayerURL();
    }, [selectedPlaylist])

    
    return (
        <div className="playlist_manager">
            <div className="playlist_menu">
                <div className="big_title">
                    <img src="./images/youtube_logo.png" id="logo" alt="Youtube Logo" />  
                </div>  
                <CreatePlaylist CallbackChangedPlaylistsList = {CallbackChangedPlaylistsList}/>
                <div className="playlists_list">
                    <ul>
                    {playlistsList
                        .sort() //alphanumeric order
                        .map((playlistName, index) => {
                        return (
                        <li key={playlistName}>
                            <Playlist                        
                            playlistName={playlistName} 
                            index={playlistsList.length - index} 
                            isSaved={isSaved}
                            CallbackChangedPlaylistsList = {CallbackChangedPlaylistsList}
                            CallbackSelectedPlaylist = {CallbackSelectedPlaylist}
                            CallbackSavedPlaylist={CallbackSavedPlaylist}
                            CallbackChangedPlaylist={CallbackChangedPlaylist}
                            selected={(playlistName===selectedPlaylist.name) ? true : false}
                            />
                        </li>
                        );
                        })}
                    </ul>
                </div>
            </div>
            <div className="playlist_viewer">
                <div className="searchspace">
                    <form onSubmit={(e) => handleSubmit(e)} className="searchbar">
                        <input
                            onChange={(e) => setSearch(e.target.value)}
                            type="text"
                            placeholder="Your new Youtube video search"
                            value={search}
                            className="textbar"
                            autoFocus
                            style={{ background: error ? "#ffb3b3" : "#ffdcdc" }}
                        />
                        <input type="submit" className="submit" value="Add video" />
                        {error ? <p>No result !</p> : <p></p>}
                    </form>
                </div>

                <div className="player_container">
                    <iframe 
                    title="video_player" 
                    height="400" width="600" allowFullScreen 
                    src={playerUrl}>
                    </iframe>
                </div>
                <div className="playlist_header">
                    {(selectedPlaylist.name) ? 
                        <SavePlaylist 
                        selectedPlaylist={selectedPlaylist.musics} 
                        selectedPlaylistName={selectedPlaylist.name}
                        isSaved={isSaved}
                        CallbackSavedPlaylist={CallbackSavedPlaylist}/> 
                        : <p></p>}
                    {selectedPlaylist.name ? 
                    <h3>{selectedPlaylist.name}</h3> 
                    : <p></p>}        
                </div>
                <ul className="playlist">
                    {selectedPlaylist.musics
                    .sort((a, b) => b.date - a.date)
                    .map((music, index) => {
                        return (
                        <li key={music.id.videoId}>
                            <Music  music={music} 
                            index={selectedPlaylist.musics.length - index }
                            playerUrl={playerUrl}
                            CallbackUpdatedUrl={CallbackUpdatedUrl} 
                            CallbackChangedPlaylist={CallbackChangedPlaylist}
                            CallbackSavedPlaylist={CallbackSavedPlaylist} />
                        </li>
                        );
                    })}
                </ul>            
            </div>
        </div>
        
    );
};

export default PlaylistManager;