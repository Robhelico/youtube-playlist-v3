import React, { useState } from "react";

const SavePlaylist = ({ selectedPlaylist, selectedPlaylistName, isSaved, CallbackSavedPlaylist }) => {

    //Methods
    const handleSave = () => {
        localStorage.setItem(selectedPlaylistName, JSON.stringify({name:selectedPlaylistName ,musics:selectedPlaylist})); //update
        CallbackSavedPlaylist(true);
        localStorage.setItem("is_saved", JSON.stringify(true)); 
    }


    return (
    <button
        onClick={() => {handleSave();}}
        className={ isSaved ? "save_button_saved" : "save_button" }
        
    >
        { isSaved ? "Saved" : "Save playlist" }
    </button>
    );
};

export default SavePlaylist;