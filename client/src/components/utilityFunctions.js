export const urlFormatter = (url) => {
  if (!url){
    return ("");
  }
  else {
    let index = 0;
    let char = url[0];
    while (char !== '='){
      char = url[index++];
    }
    return("https://youtube.com/embed/" + url.slice(index));
  }
}

export const getLocalStorage = (localStorageName, dataType=undefined) => { //get the parsed data from the localStorage. If the data doesn't exist, return a default value matching the specified dataType. Create also the new key-value in localStorage

  const currentData = JSON.parse(localStorage.getItem(localStorageName));

  if(currentData!=='' && currentData!== null && currentData!==undefined){//if saved data from browser not empty, get it
    return (currentData);
  }
  else{//else, create new data storage
    let emptyValue = 0;

    switch (dataType){ //defaultValue sent if no data in localStorage
      case "int" :
        emptyValue = 0;
        break;
      case "arr" :
        emptyValue = [];
        break;
      case "str" :
        emptyValue = "";
        break;
      case "obj" : 
        emptyValue = {};
        break;
      case "playlist" : 
        emptyValue = {name:"", musics:[]};
      break;
      case "bool" : 
        emptyValue = false;
      break;
      default :
        emptyValue=undefined;
    }
    
    localStorage.setItem(localStorageName,JSON.stringify(null));
    return emptyValue;
  }
}