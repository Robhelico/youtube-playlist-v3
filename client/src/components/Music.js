import React from "react";
import { urlFormatter, getLocalStorage } from "./utilityFunctions";

const Music = ({ music, index, playerUrl, CallbackUpdatedUrl, CallbackChangedPlaylist, CallbackSavedPlaylist }) => {

  //Methods
  const dateParser = (date) => {
    let newDate = new Date(date).toLocaleDateString("en-US", {
      year: "numeric",
      month: "numeric",
      day: "numeric"//,
      // hour: "numeric",
      // minute: "numeric",
    });
    return newDate;
  };


  const handlePlay = () => { //set video player with the url of the music

    const formatted_url = urlFormatter(music.url);
    localStorage.setItem("current_url", JSON.stringify(formatted_url));
    CallbackUpdatedUrl(formatted_url);

  };


  const handleRemove = () => { //remove the music from the playlist

    let currentPlaylist = getLocalStorage("selected_playlist","obj");
    let currentMusicsList = currentPlaylist.musics;

    let ptId = currentMusicsList[0].id.videoId; //finding the index in the playlist of the music that has to be removed
    let index = 0;

    while (ptId !== music.id.videoId){
      index++;
      ptId = currentMusicsList[index].id.videoId;
    }
    
    const currentUrl = JSON.parse(localStorage.getItem("current_url"));//if the music removed is displayed in the player, display nothing

    if(urlFormatter(currentMusicsList[index].url)  === currentUrl) {
      localStorage.setItem("current_url", '""');
    }

    currentMusicsList.splice(index, 1); //music removed and new playlist stored in localStorage
    currentPlaylist.musics = currentMusicsList;
    CallbackChangedPlaylist(currentPlaylist);
    localStorage.setItem("selected_playlist", JSON.stringify(currentPlaylist));
    CallbackSavedPlaylist(false);
    localStorage.setItem("is_saved", JSON.stringify(false));

  };

  return (
    
    <div className={ (urlFormatter(music.url)===playerUrl) ? "selected_music" : "music" } >
      <div className="music_header">
        <div className="index">{index}</div>
        <img src={music.snippet.thumbnails.url} alt="video thumbnail"/>
        <div className="title">{music.title}</div>
        <div className="duration_raw">{music.duration_raw}</div>
      </div>

      <div className="btn_container">
        <button
          onClick={() => {handlePlay();}}
          className="play_button">
          ►
        </button>
        <button
          onClick={() => {
            if (window.confirm("Remove this music from the playlist ?")) {
              handleRemove();
            }
          }}
          className="remove_button">
          ✖
        </button>
      </div>

      <div>
        <a href={music.url} target="_blank" rel="noreferrer">Original video</a>
        <br />
        <em>Added on {dateParser(music.date)}</em>
      </div>
    </div>
  );
};

export default Music;